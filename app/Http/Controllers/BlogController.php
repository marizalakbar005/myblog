<?php

namespace App\Http\Controllers;
use App\blog;
use App\category;
use Illuminate\Http\Request;

class blogController extends Controller
{
    //
    public function index()
    {
        $data = blog::all();
        return view('child.index',compact('data'));
    }

    public function create ()
    {
        $data = category::all();
        return view('child.create_blog',compact('data'));
    }

    public function store(Request $request)
    {
        $data = new blog();
        
        if($request->hasFile('gambar')){
            $filenameWithExt = $request->file('gambar')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('gambar')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('gambar')->storeAs('public/img', $file_store_name);
        }else{
            $file_store_name = 'noimage.jpg';
        }
        $data->category_id = $request->category_id;
        $data->title = $request->title;
        $data->content = $request->content;
        $data->gambar = $file_store_name;
        $data->save();
        
        return redirect('/');
    }

    public function edit($id)
    {
        $data = blog::find($id);
        $category = category::where('id','!=',$data->category_id)->get();
        return view('child.edit',compact('data','category'));
    } 
    
    public function update(Request $request,$id)
    {
        $data = blog::find($id);
        // handle upload image
        $old_cover = $request->get('old_gambar');
        if($request->hasFile('gambar')){
            $filenameWithExt = $request->file('gambar')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('gambar')->getClientOriginalExtension();
            $file_store_name = $filename.'_'.time().'.'.$extension;
            $path = $request->file('gambar')->storeAs('public/img', $file_store_name);
            $old_path = 'storage/img/'.$old_cover;
            if(!isset($old_path)) {
                unlink($old_path);
            }
        } else {
            $file_store_name = $old_cover;
        }
        $data->category_id = $request->category_id;
        $data->title = $request->title;
        $data->content = $request->content;
        $data->gambar = $file_store_name;
        $data->save();

        return redirect('/');
    }

    public function show($id)
    {
        $data = blog::find($id);
        return view('child.detail',compact('data'));
    }

    public function destroy($id)
    {
        $data = blog::find($id);
        $path = 'storage/img/'.$data->gambar;
        if (file_exists($path)){
            unlink($path);    
        } 
        $data->delete();
        return redirect('/');
    }
}
