<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    //
    public function blog(){
        return $this->belongsTo('App\Blog','category_id');
    }

}
