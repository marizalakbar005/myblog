<?php

use Illuminate\Database\Seeder;
use App\category;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        category::create([
            'name'=> 'Technology',
        ]);
        category::create([
            'name'=> 'Creative',
        ]);
        category::create([
            'name'=> 'Design',
        ]);

    }
}
