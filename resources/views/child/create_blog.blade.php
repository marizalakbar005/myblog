@extends('parent')
@section('title','Create Blog')

@section('body')

<form method="POST" action="/create" enctype="multipart/form-data">
  
  @csrf
    <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Category</label>
    <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="category_id">
      @foreach ($data as $item)
    <option value="{{$item->id}}">{{$item->name}}</option>
      @endforeach
    </select>
    
  
    <label>Gambar</label><br>
    <input type="file" name="gambar" class="form-control">
  <div>
    <label>Title</label>
    <input type="text" class="form-control" name="title">
  </div>
  <div class="form-group">
    <label>Content</label>
    <input type="text" class="form-control" name="content">
  </div>
  <button type="submit" class="btn btn-primary">Simpan</button>
  <a href="/" class="btn btn-danger">Kembali</a>
</form>

@endsection
