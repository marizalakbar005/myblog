@extends('parent')
@section('title','Create Blog')

@section('body')

<div class="mb-4">
    <a href="/" class="btn btn-primary">Kembali</a>
</div>
<div class="card">
  <div class="card-body">
    <img src="{{('/storage/img/').$data->gambar.''}}" style="width: 30rem;" height="200 px" class="rounded mx-auto d-block"><hr>    
    <h5 class="card-title">{{$data->title}}</h5>
    
    <p class="float-md-right">{{$data->category->name}}</p><br>
    <p class="card-text">{{$data->content}}</p>
  </div>
</div>

@endsection