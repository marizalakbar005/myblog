@extends('parent')
@section('title','Edit Blog')

@section('body')
<form method="POST" action="/edit/{{$data->id}}" enctype="multipart/form-data">
  @csrf
    <div class="row">
      <div class="col-md-6">
          <img src="{{asset('/storage/img/'.$data->gambar.'')}}" alt="" style="width: 100%; max-width: 420px">
          <p>Gambar Name: {{$data->gambar}}</p>
          <div class="form-group" style="margin-top: 21px">
              <label  for="">Change Gambar</label>
              <input type="file" class="form-control" name="gambar" >
              <input type="hidden" value="{{$data->gambar}}" class="form-control" name="old_gambar">
          </div>
      </div>
    </div>

  <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Preference</label>
  <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="category_id">
    <option value="{{$data->id}}">{{$data->category->name}}</option>
    @foreach ($category as $item)
    <option value="{{$item->id}}">{{$item->name}}</option>
    @endforeach
  </select>
  
  <input type="hidden" name="_method" value="PATCH">
  <div class="form-group">
    <label>Title</label>
    <input type="text" class="form-control" name="title" value="{{$data->title}}">
  </div>
  <div class="form-group">
    <label>Content</label>
    <input type="text" class="form-control" name="content" value="{{$data->content}}">
  </div>
  <button type="submit" class="btn btn-primary">Simpan</button>
  <a href="/" class="btn btn-danger">Kembali</a>
</form>
@endsection