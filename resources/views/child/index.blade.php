@extends('parent')
@section('title','index')

@section('body')

<div class="mb-4"> 
  <a href="/create" class="btn btn-primary">Create Blog</a>
</div>

<div class="row">
  @foreach ($data as $item)
    <div class="card col-3 m-2"> 
      <img src="{{('/storage/img/').$item->gambar.''}}" width="auto" height="200">
      <div class="card-body">
        <div class="row">
          <div class="col-6">
            <a href="/show/{{$item->id}}"><h5 class="card-title float-md-left">{{$item->title}}</h5></a>
          </div>
          <div class="col-6">
            <p class="float-md-right">{{$item->category->name}}</p>
          </div>
        </div>
        <p class="card-text">{{$item->content}}</p>
      <div class="row">
        <div class="col-6">
          <a href="/edit/{{$item->id}}" class="btn btn-primary form-control">Edit</a>
        </div>
        <div class="col-6">
          <form method="POST" action="/destroy/{{$item->id}}">
            @csrf
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-warning form-control">Delete</button>
          </form>
        </div>
      </div>

      </div>
    </div>

  @endforeach
</div>
@endsection