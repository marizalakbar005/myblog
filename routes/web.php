<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','BlogController@index')->name('index-blog');
Route::get('/create','BlogController@create')->name('create-blog');
Route::post('/create','BlogController@store')->name('store-blog');
Route::get('edit/{id}','BlogController@edit')->name('edit-blog');
Route::patch('/edit/{id}','BlogController@update')->name('update-blog');
Route::get('/show/{id}','BlogController@show')->name('show-blog');
Route::delete('/destroy/{id}','BlogController@destroy')->name('destroy-blog');
